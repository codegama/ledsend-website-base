<?php 

namespace App\Helpers;

use Mailgun\Mailgun;

use Validator, Hash, Exception, Auth, Mail, File, Log, Storage, Setting, DB;

use App\Admin, App\User, App\Settings, App\StaticPage;

class Helper {

    public static function clean($string) {

        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public static function generate_token() {
        
        return Helper::clean(Hash::make(rand() . time() . rand()));
    }

    public static function generate_token_expiry() {

        $token_expiry_hour = Setting::get('token_expiry_hour') ? Setting::get('token_expiry_hour') : 1;
        
        return time() + $token_expiry_hour*3600;  // 1 Hour
    }

    // Note: $error is passed by reference
    
    public static function is_token_valid($entity, $id, $token, &$error) {

        if (
            ( $entity== USER && ($row = User::where('id', '=', $id)->where('token', '=', $token)->first()) )
        ) {

            if ($row->token_expiry > time()) {
                // Token is valid
                $error = NULL;
                return true;
            } else {
                $error = ['success' => false, 'error' => api_error(1003), 'error_code' => 1003];
                return FALSE;
            }
        }

        $error = ['success' => false, 'error' => api_error(1004), 'error_code' => 1004];
        return FALSE;
   
    }

    public static function generate_email_code($value = "") {

        return mt_rand(100000, 999999);
    }

    public static function generate_email_expiry() {

        $token_expiry = Setting::get('token_expiry_hour') ?: 1;
            
        return time() + $token_expiry*3600;  // 1 Hour

    }

    // Check whether email verification code and expiry

    public static function check_email_verification($verification_code , $user_id , &$error) {

        // if(!$user_id) {

        //     $error = tr('user_id_empty');

        //     return FALSE;

        // } else {

        //     $user_details = User::find($user_id);
        // }

        // // Check the data exists

        // if($user_details) {

        //     // Check whether verification code is empty or not

        //     if($verification_code) {

        //         // Log::info("Verification Code".$verification_code);

        //         // Log::info("Verification Code".$user_details->verification_code);

        //         if ($verification_code ===  $user_details->verification_code ) {

        //             // Token is valid

        //             $error = NULL;

        //             // Log::info("Verification CODE MATCHED");

        //             return true;

        //         } else {

        //             $error = tr('verification_code_mismatched');

        //             // Log::info(print_r($error,true));

        //             return FALSE;
        //         }

        //     }
                
        //     // Check whether verification code expiry 

        //     if ($user_details->verification_code_expiry > time()) {

        //         // Token is valid

        //         $error = NULL;

        //         Log::info(tr('token_expiry'));

        //         return true;

        //     } else if($user_details->verification_code_expiry < time() || (!$user_details->verification_code || !$user_details->verification_code_expiry) ) {

        //         $user_details->verification_code = Helper::generate_email_code();
                
        //         $user_details->verification_code_expiry = Helper::generate_email_expiry();
                
        //         $user_details->save();

        //         // If code expired means send mail to that user

        //         $subject = tr('verification_code_title');
        //         $email_data = $user_details;
        //         $page = "emails.welcome";
        //         $email = $user_details->email;
        //         $result = Helper::send_email($page,$subject,$email,$email_data);

        //         $error = tr('verification_code_expired');

        //         Log::info(print_r($error,true));

        //         return FALSE;
        //     }
       
        // }

        return TRUE;

    }
    
    public static function generate_password() {

        $new_password = time();

        $new_password .= rand();

        $new_password = sha1($new_password);

        $new_password = substr($new_password, 0,8);

        return $new_password;
    
    }

    public static function file_name() {

        $file_name = time();

        $file_name .= rand();

        $file_name = sha1($file_name);

        return $file_name;    
    }

    public static function upload_file_old($picture , $folder_path = COMMON_FILE_PATH) {

        $file_path_url = "";

        $file_name = Helper::file_name();

        $ext = $picture->extension();

        $local_url = $file_name . "." . $ext;

        $inputFile = base_path('public'.$folder_path.$local_url);

        $picture->move(public_path().$folder_path, $local_url);

        $file_path_url = Helper::web_url().$folder_path.$local_url;

        return $file_path_url;
    
    }


    public static function delete_file_old($picture, $path = COMMON_FILE_PATH) {

        if(file_exists(public_path().$path.basename($picture))) {

            File::delete(public_path().$path.basename($picture));
      
        } else {

            return false;
        }  

        return true;    
    
    }
 
    public static function send_email($page,$subject,$email,$email_data) {

        // check the email notification

        if(Setting::get('is_email_notification') == YES) {

            // Don't check with envfile function. Because without configuration cache the email will not send

            if(config('mail.username') &&  config('mail.password')) {

                try {

                    $site_url = url('/');

                    $isValid = 1;

                    if(envfile('MAIL_DRIVER') == 'mailgun' && Setting::get('MAILGUN_PUBLIC_KEY')) {

                        Log::info("isValid - start");

                        # Instantiate the client.

                        $email_address = new Mailgun(Setting::get('MAILGUN_PUBLIC_KEY'));

                        $validateAddress = $email;

                        # Issue the call to the client.
                        $result = $email_address->get("address/validate", ['address' => $validateAddress]);

                        # is_valid is 0 or 1

                        $isValid = $result->http_response_body->is_valid;

                        Log::info("isValid FINAL STATUS - ".$isValid);

                    }

                    if($isValid) {

                        if (Mail::queue($page, ['email_data' => $email_data,'site_url' => $site_url], 
                                function ($message) use ($email, $subject) {

                                    $message->to($email)->subject($subject);
                                }
                        )) {

                            $message = api_success(102);

                            $response_array = ['success' => true , 'message' => $message];

                            return json_decode(json_encode($response_array));

                        } else {

                            throw new Exception(api_error(116) , 116);
                            
                        }

                    } else {

                        $error = api_error(115);

                        throw new Exception($error, 115);                  

                    }

                } catch(\Exception $e) {

                    $error = $e->getMessage();

                    $error_code = $e->getCode();

                    $response_array = ['success' => false , 'error' => $error , 'error_code' => $error_code];
                    
                    return json_decode(json_encode($response_array));

                }
            
            } else {

                $error = api_error(106);

                $response_array = ['success' => false , 'error' => $error , 'error_code' => 106];
                    
                return json_decode(json_encode($response_array));

            }
        
        } else {
            Log::info("email notification disabled by admin");
        }
    
    }

    public static function custom_validator($request, $request_inputs, $custom_errors = []) {

        $validator = Validator::make($request, $request_inputs, $custom_errors);

        if($validator->fails()) {

            $error = implode(',', $validator->messages()->all());

            throw new Exception($error, 101);
               
        }
    }

    /**
     * @method settings_generate_json()
     *
     * @uses used to update settings.json file with updated details.
     *
     * @created vidhya
     * 
     * @updated vidhya
     *
     * @param -
     *
     * @return boolean
     */
    
    public static function settings_generate_json() {
        
        $settings = Settings::get();

        $sample_data = [];

        foreach ($settings as $key => $setting_details) {

            $sample_data[$setting_details->key] = $setting_details->value;
        }
        
        $static_page_ids1 = ['about', 'terms', 'privacy', 'contact'];

        $footer_pages1 = StaticPage::select('id as page_id', 'unique_id', 'type as page_type', 'title')->whereIn('type', $static_page_ids1)->where('status', APPROVED)->get();

        $static_page_ids1 = ['help', 'faq', 'others'];

        $footer_pages2 = StaticPage::select('id as page_id', 'unique_id', 'type as page_type', 'title')->whereIn('type', $static_page_ids1)->where('status', APPROVED)->skip(0)->take(4)->get();

        $sample_data['footer_pages1'] = $footer_pages1;

        $sample_data['footer_pages2'] = $footer_pages2;

        $data['data'] = $sample_data;

        $data = json_encode($data);
        
        $folder_path_name = 'default-json/settings.json';

        Storage::disk('public')->put($folder_path_name, $data);
   
    }

    /**
     * @method upload_file
     */
    
    public static function storage_upload_file($input_file, $folder_path = COMMON_FILE_PATH) {

        $url = "";

        if($input_file) {

            $name = Helper::file_name();

            $ext = $input_file->extension();

            $file_name = $name.".".$ext;

            $public_folder_path = "public/".$folder_path;

            Storage::putFileAs($public_folder_path, $input_file, $file_name);

            $storage_file_path = $folder_path.$file_name;

            $url = asset('storage/'.$storage_file_path);
        }

        return $url;

    }

    /**
     * @method
     * 
     */
    public static function storage_delete_file($url, $folder_path = COMMON_FILE_PATH) {

        $file_name = basename($url);

        $storage_file_path = $folder_path.$file_name;

        Storage::delete($storage_file_path);
    }


    public static function delete_file($picture, $path = COMMON_FILE_PATH) {

        if ( file_exists( public_path() . $path . basename($picture))) {

            File::delete( public_path() . $path . basename($picture));
      
        } else {

            return false;
        }  

        return true;    
    }


    

}