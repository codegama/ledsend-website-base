<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Setting, DB;

class User extends Authenticatable
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $appends = ['is_notification'];

    public function getIsNotificationAttribute() {

        return $this->email_notification_status;
    }

    /**
     * Get the UserCard record associated with the user.
     */
    public function userCards() {
        
        return $this->hasMany(UserCard::class, 'user_id');
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query) {

        return $query->where('users.is_kyc_document_approved', USER_KYC_DOCUMENT_APPROVED)->where('users.status', USER_APPROVED)->where('users.is_verified', USER_EMAIL_VERIFIED);
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommonResponse($query) {

        return $query->select(
            'users.id as user_id',
            'users.unique_id as user_unique_id',
            'users.*'
            );
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOtherResponse($query) {

        return $query->select(
            'users.id as user_id',
            'users.unique_id as user_unique_id',
            'users.username',
            'users.name',
            'users.email',
            'users.picture',
            'users.mobile',
            'users.created_at',
            'users.updated_at'
            );
    
    }
    
    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['name'] = $model->attributes['first_name']." ".$model->attributes['last_name'];

            
            if (Setting::get('is_account_email_verification') == YES && envfile('MAIL_USERNAME') && envfile('MAIL_PASSWORD')) { 

                $model->generateEmailCode();            

            } else {
                
                $model->attributes['is_verified'] = USER_EMAIL_VERIFIED;

            }

            $model->attributes['payment_mode'] = COD;

            $model->attributes['unique_id'] = uniqid();

            $model->attributes['token'] = \Helper::generate_token();

            $model->attributes['token_expiry'] = \Helper::generate_token_expiry();

            $model->attributes['status'] = USER_APPROVED;

            if(in_array($model->attributes['login_by'], ['facebook', 'google', 'apple', 'linkedin', 'instagram'] )) {
                
                $model->attributes['password'] = \Hash::make($model->attributes['social_unique_id']);
            }

        });

        static::created(function($model) {

            $model->attributes['email_notification_status'] = $model->attributes['push_notification_status'] = YES;

            $model->attributes['username'] = routefreestring($model->attributes['name'].$model->attributes['id']);

            $model->attributes['unique_id'] = "UID"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

        static::updating(function($model) {

            $model->attributes['username'] = routefreestring($model->attributes['name']);

            $model->attributes['name'] = $model->attributes['first_name']." ".$model->attributes['last_name'];

        });

        static::deleting(function ($model){

            \Helper::delete_file($model->picture , PROFILE_PATH_USER);

        });

    }

    /**
     * Generates Token and Token Expiry
     * 
     * @return bool returns true if successful. false on failure.
     */

    protected function generateEmailCode() {

        $this->attributes['verification_code'] = \Helper::generate_email_code();

        $this->attributes['verification_code_expiry'] = \Helper::generate_email_expiry();

        // Check Email verification controls and email configurations

        if(Setting::get('is_account_email_verification') == YES && Setting::get('is_email_notification') == YES && Setting::get('is_email_configured') == YES) {
            
            if($this->attributes['login_by'] != 'manual') {
                
                $this->attributes['is_verified'] = USER_EMAIL_VERIFIED;

            } else {
                
                $this->attributes['is_verified'] = USER_EMAIL_NOT_VERIFIED;
            }

        } else { 

            $this->attributes['is_verified'] = USER_EMAIL_VERIFIED;
        }
        
        return true;
    
    }

}
