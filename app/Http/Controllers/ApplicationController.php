<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Helper;

use Log, Validator, Exception, DB, Hash, PDF, Setting;


use App\User;


class ApplicationController extends Controller
{
    public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("ApplicationController Request Data".print_r($request->all(), true));

    }
    /**
     * @method static_pages_api()
     *
     * @uses used to get the pages
     *
     * @created Vidhya R 
     *
     * @edited Vidhya R
     *
     * @param - 
     *
     * @return JSON Response
     */

    public function static_pages_api(Request $request) {

        $base_query =  \App\StaticPage::where('status', APPROVED)->CommonResponse();

        if($request->page_type) {

            $static_page = $base_query->where('type', $request->page_type)->first();

            $data = $static_page;

        } elseif($request->unique_id) {

            $static_page = $base_query->where('unique_id', $request->unique_id)->first();

            $data = $static_page;

        } else {

            $static_pages = $base_query->orderBy('title', 'asc')->get();

            $data = $static_pages ? $static_pages->toArray(): [];

        }

        return $this->sendResponse($message = "", $code = "", $data);

    }


    /**
     * @method email_verify()
     *
     * @uses To verify the email from user.  
     *
     * @created Bhawya
     *
     * @updated Bhawya
     *
     * @param -
     *
     * @return JSON RESPONSE
     */

    public function email_verify(Request $request) {

        if($request->user_id) {

            $user_details = User::find($request->user_id);

            if(!$user_details) {

                return redirect()->away(Setting::get('frontend_url'))->with('flash_error',tr('user_details_not_found'));
            } 

            if($user_details->is_verified == USER_EMAIL_VERIFIED) {

                return redirect()->away(Setting::get('frontend_url'))->with('flash_success' ,tr('user_verify_success'));
            }

            $response = Helper::check_email_verification($request->verification_code , $user_details->id, $error, USER);
            
            if($response) {

                $user_details->is_verified = USER_EMAIL_VERIFIED;       

                $user_details->save();

            } else {

                return redirect()->away(Setting::get('frontend_url'))->with('flash_error' , $error);
            }

        } else {

            return redirect()->away(Setting::get('frontend_url'))->with('flash_error' , $error);


        }

    }
}
