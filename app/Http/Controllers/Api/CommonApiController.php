<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use DB, Log, Hash, Validator, Exception, Setting, Helper;

use App\User;

class CommonApiController extends Controller
{
    protected $loginUser, $skip, $take;

	public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::CommonResponse()->find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

    }

    /**
     * @method dashboard()
     * 
     * @uses used to get the invoices list based on the logged in user
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function dashboard(Request $request) {

        try {

        	$data = new \stdClass;

        	$base_query = \App\GeneratedInvoice::where('sender_id', $request->sender_id)->CommonResponse();

        	$data->total_invoices = $base_query->count();

        	$data->paid_invoices =  $base_query->where('status', INVOICE_PAID)->count();

            $data->draft_invoices =  $base_query->where('status', INVOICE_DRAFT)->count();

        	$data->unpaid_invoices =  $base_query->where('status', INVOICE_SENT)->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }

	}
}
