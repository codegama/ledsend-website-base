<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use DB, Log, Hash, Validator, Exception, Setting, Helper;

use App\User;

use App\Repositories\PaymentRepository as PaymentRepo;

use App\Repositories\WalletRepository as WalletRepo;

class InvoiceApiController extends Controller
{
    protected $loginUser, $skip, $take;

	public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::CommonResponse()->find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

        $request->request->add(['timezone' => $this->timezone]);

    }

    /**
     * @method generated_invoices_index()
     * 
     * @uses used to get the invoices list based on the logged in user
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function generated_invoices_index(Request $request) {

        try {

            $base_query = $total_query = \App\GeneratedInvoice::where('sender_id', $request->id)->orWhere('to_user_id', $request->id);

            $data['invoices'] = $base_query->orderBy('generated_invoices.id', 'desc')->skip($this->skip)->take($this->take)->CommonResponse()->get();

            $data['total'] = $total_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }

	}

    /**
     * @method generated_invoices_drafts()
     * 
     * @uses used to get the invoices list based on the logged in user
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function generated_invoices_drafts(Request $request) {

        try {

            $base_query = $total_query = \App\GeneratedInvoice::where('sender_id', $request->id)->where('generated_invoices.status', INVOICE_DRAFT);

            $data['invoices'] = $base_query->orderBy('generated_invoices.id', 'desc')->skip($this->skip)->take($this->take)->CommonResponse()->get();

            $data['total'] = $total_query->count();
            
            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method generated_invoices_scheduled()
     * 
     * @uses used to get the invoices list based on the logged in user
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function generated_invoices_scheduled(Request $request) {

        try {

            $base_query = $total_query = \App\GeneratedInvoice::where('sender_id', $request->id)->where('generated_invoices.status', INVOICE_SCHEDULED);

            $data['invoices'] = $base_query->skip($this->skip)->take($this->take)->CommonResponse()->get();

            $data['total'] = $total_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method generated_invoices_sent()
     * 
     * @uses used to get the invoices list based on the logged in user
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function generated_invoices_sent(Request $request) {

        try {

            $base_query = $total_query = \App\GeneratedInvoice::where('sender_id', $request->id)->where('generated_invoices.status', INVOICE_SENT);

            $data['invoices'] = $base_query->skip($this->skip)->take($this->take)->CommonResponse()->get();

            $data['total'] = $total_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }
    
    /**
     * @method generated_invoices_paid()
     * 
     * @uses used to get the invoices list based on the logged in user
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function generated_invoices_paid(Request $request) {

        try {

            $base_query = $total_query = \App\GeneratedInvoice::where('sender_id', $request->id)->where('generated_invoices.status', INVOICE_PAID);

            $data['invoices'] = $base_query->skip($this->skip)->take($this->take)->CommonResponse()->get();

            $data['total'] = $total_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method generated_invoices_unpaid()
     * 
     * @uses used to get the invoices list based on the logged in user
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function generated_invoices_unpaid(Request $request) {

        try {

            $base_query = $total_query = \App\GeneratedInvoice::where('sender_id', $request->id)->whereIn('generated_invoices.status', [INVOICE_SCHEDULED, INVOICE_SENT]);

            $data['invoices'] = $base_query->skip($this->skip)->take($this->take)->CommonResponse()->get();

            $data['total'] = $total_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method generated_invoices_received()
     * 
     * @uses used to get the invoices list based on the logged in user
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function generated_invoices_received(Request $request) {

        try {

            $base_query = $total_query = \App\GeneratedInvoice::where('to_user_id', $request->id);

            $data['invoices'] = $base_query->skip($this->skip)->take($this->take)->CommonResponse()->get();

            $data['total'] = $total_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method generated_invoices_view()
     * 
     * @uses used to get the invoices list based on the logged in user
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function generated_invoices_view(Request $request) {

        try {

            // Validation start

            $rules = ['generated_invoice_id' => 'required|exists:generated_invoices,id'];

            $custom_errors = ['generated_invoice_id.exists' => api_error(129)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $invoice_details = \App\GeneratedInvoice::where('generated_invoices.id', $request->generated_invoice_id)->first();

            if(!$invoice_details) {

                throw new Exception(api_error(129), 129);
                
            }

            $data['invoice_details'] = $invoice_details;

            $data['send_btn_status'] = $invoice_details->status == INVOICE_DRAFT ? YES: NO;

            $data['cancel_btn_status'] = $invoice_details->status == INVOICE_SENT ? YES: NO;

            $data['payment_btn_status'] = $invoice_details->status == INVOICE_SENT ? YES: NO;

            $data['invoice_items'] =\App\GeneratedInvoiceItem::where('generated_invoice_id', $request->generated_invoice_id)->CommonResponse()->get();

            $data['invoice_billing_info'] =\App\GeneratedInvoiceInfo::where('generated_invoice_id', $request->generated_invoice_id)->CommonResponse()->first();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method generated_invoices_draft()
     * 
     * @uses Delete user account based on user id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function generated_invoices_draft(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start
            
            $rules = [
                    'generated_invoice_id' => 'nullable|exists:generated_invoices,id',
                    'logo' => 'nullable|file|mimes:jpg,png,jpeg', // 10MB
                    'invoice_number' => 'required',
                    'invoice_date'=> 'date_format:Y-m-d',
                    'due_date'=> 'date_format:Y-m-d',
            ];


            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $request->request->add(['sender_id' => $request->id]);

            $invoice_details = \App\GeneratedInvoice::firstOrNew(['id' => $request->generated_invoice_id], $request->all());

            $invoice_details->reference = $request->reference ?: ""; 

            $invoice_details->notes = $request->notes ?: ""; 

            $invoice_details->terms_conditions = $request->terms_conditions ?: "";

            if($request->file('logo')) {

                if($request->generated_invoice_id) {

                    Helper::storage_delete_file($invoice_details->logo, COMMON_FILE_PATH);
                }

                $invoice_details->logo = Helper::storage_upload_file($request->file('logo'), COMMON_FILE_PATH);

            } 

            $invoice_details->title = $request->title ?: '';

            $invoice_details->user_wallet_payment_id = 0;

            $invoice_details->due_date = $request->due_date ? date('Y-m-d H:i:s', strtotime($request->due_date)) : date('Y-m-d H:i:s');

            if($request->to_user_email_address) {

                $invoice_details->to_user_email_address = $request->to_user_email_address ?: "";

                $to_user_details = User::where('email', $request->to_user_email_address)->first();

                $invoice_details->to_user_id = $to_user_details->id ?? 0;

            }

            if($request->cc_user_email_address) {

                $invoice_details->cc_user_email_address = $request->cc_user_email_address ?: "";

                $cc_user_details = User::where('email', $request->cc_user_email_address)->first();

                $invoice_details->cc_user_id = $cc_user_details->id ?? 0;

            }

            $invoice_details->invoice_date = $request->invoice_date ? date('Y-m-d H:i:s', strtotime($request->invoice_date)) : date('Y-m-d H:i:s');

            $invoice_details->status = INVOICE_DRAFT;

            $invoice_details->save();
                
            DB::commit();

            return $this->sendResponse(api_success(118), 118, $invoice_details);


        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

	/**
     * @method generated_invoices_info_save()
     * 
     * @uses Delete user account based on user id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function generated_invoices_info_save(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
            		'generated_invoice_id' => 'nullable|exists:generated_invoices,id',
            		'generated_invoice_info_id' => 'nullable|exists:generated_invoice_infos,id',
            		'email' => 'nullable|email|max:255', // 10MB
            		'mobile' => 'nullable|digits_between:6,13',
            	];

            $custom_errors = ['generated_invoice_id.exists' => api_error(129)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            // Invoice start

            $request->request->add(['sender_id' => $request->id]);

            if(!$request->generated_invoice_id) {

                $invoice_details = new \App\GeneratedInvoice;

                $invoice_details->sender_id = $request->id;

                $invoice_details->title = $invoice_details->invoice_number = rand();

                $invoice_details->save();

                $request->request->add(['generated_invoice_id' => $invoice_details->id]);

            } else {
                
                $invoice_details = \App\GeneratedInvoice::where('id', $request->generated_invoice_id)->first();

            }

            // Invoice end

            $info_details = \App\GeneratedInvoiceInfo::where(['generated_invoice_id' => $request->generated_invoice_id, 'id' => $request->generated_invoice_info_id])->first() ?? new \App\GeneratedInvoiceInfo;

            $info_details->generated_invoice_id = $request->generated_invoice_id;

            $info_details->business_name = $request->business_name ?? "";

            $info_details->website = $request->website ?? "";

            $info_details->email = $request->email ?? "";

            $info_details->mobile = $request->mobile ?? "";

            $info_details->first_name = $request->first_name ?? "";

            $info_details->last_name = $request->last_name ?? "";

            $info_details->address = $request->address ?? "";

            $info_details->tax_number = $request->tax_number ?? "";

            $info_details->additional_information = $request->additional_information ?? "";

            $info_details->save();
                
            DB::commit();

            return $this->sendResponse(api_success(120), 120, $info_details);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

	}

	/**
     * @method generated_invoices_items()
     * 
     * @uses Delete user account based on user id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function generated_invoices_items(Request $request) {

        try {

            // Validation start

            $rules = ['generated_invoice_id' => 'required|exists:generated_invoices,id'];

            $custom_errors = ['generated_invoice_id.exists' => api_error(129)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $invoice_items = \App\GeneratedInvoiceItem::where('generated_invoice_id', $request->generated_invoice_id)->get();

            $data['invoice_items'] = $invoice_items;

            $data['total'] = $invoice_items->count() ?: 0;
           
            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }

	}

    /**
     * @method generated_invoices_items_save()
     * 
     * @uses Delete user account based on user id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function generated_invoices_items_save(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
                    'generated_invoice_id' => 'nullable|exists:generated_invoices,id',
                    'generated_invoice_item_id' => 'nullable|exists:generated_invoice_items,id',
                    'name' => 'required|max:100',
                    'amount' => 'required|numeric|min:0',
                    'quantity' => 'required|numeric|min:0',
                    'tax_price' => 'nullable|numeric|min:0'
                ];

            $custom_errors = ['generated_invoice_id.exists' => api_error(129)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $request->request->add(['sender_id' => $request->id]);

            $invoice_details = \App\GeneratedInvoice::where('id', $request->generated_invoice_id)->first();

            if(!$request->generated_invoice_id) {

                $invoice_details = new \App\GeneratedInvoice;

                $invoice_details->sender_id = $request->id;

                $invoice_details->title = $invoice_details->invoice_number = rand();

                $invoice_details->save();
            }

            $invoice_item_details = \App\GeneratedInvoiceItem::where('id', $request->generated_invoice_item_id)->first() ?? new \App\GeneratedInvoiceItem;

            $invoice_item_details->generated_invoice_id = $invoice_details->id;
            
            $invoice_item_details->name = $request->name ?? "";

            $invoice_item_details->amount = $request->amount ?? 0.00;

            $invoice_item_details->quantity = $request->quantity ?? 0.00;

            $invoice_item_details->tax_price = $request->tax_price ?? 0.00;

            $invoice_item_details->sub_total = $invoice_item_details->amount * $invoice_item_details->quantity;

            $invoice_item_details->total = $invoice_item_details->sub_total + $invoice_item_details->tax_price; 

            $invoice_item_details->save();

            $base_query = \App\GeneratedInvoiceItem::where('generated_invoice_id', $request->generated_invoice_id);

            $invoice_details->total = $base_query->sum('total');

            $invoice_details->sub_total = $base_query->sum('sub_total');

            $invoice_details->tax_total = $base_query->sum('tax_price');

            $invoice_details->save();
            
            DB::commit();

            $invoice_item_details->invoice_details = $invoice_details;

            return $this->sendResponse(api_success(119), 119, $invoice_item_details);


        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method generated_invoices_items_delete()
     * 
     * @uses delete the selected iteem
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function generated_invoices_items_delete(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
                    'generated_invoice_item_id' => 'required|exists:generated_invoice_items,id'
                ];

            $custom_errors = ['generated_invoice_item_id.exists' => api_error(135)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            if(\App\GeneratedInvoiceItem::where('id', $request->generated_invoice_item_id)->delete()) {
            
                DB::commit();

                return $this->sendResponse(api_success(124), 124, $request->all());
            }

            throw new Exception(api_error(136), 136);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method generated_invoices_send()
     * 
     * @uses send the generated invoice to user
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function generated_invoices_send(Request $request) {

        try {
            
            DB::beginTransaction();

            // Validation start

            $rules = ['generated_invoice_id' => 'required|exists:generated_invoices,id'];

            $custom_errors = ['generated_invoice_id.exists' => api_error(129)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $invoice_details = \App\GeneratedInvoice::where('id', $request->generated_invoice_id)->where('sender_id', $request->id)->first();

            if(!$invoice_details) {
                
                throw new Exception(api_error(129), 129);
            }

            $item_base_query = \App\GeneratedInvoiceItem::where('generated_invoice_id', $request->generated_invoice_id);

            if($item_base_query->count() <= 0) {

                throw new Exception(api_error(148), 148);

            }


            $current_date = common_date(date('Y-m-d H:i:s'), $this->loginUser->timezone, 'Y-m-d H:i:s');

            $invoice_details->status = INVOICE_SENT;

            $invoice_details->user_wallet_payment_id = 0;

            $invoice_details->total = $item_base_query->sum('total');

            $invoice_details->sub_total = $item_base_query->sum('sub_total');

            $invoice_details->tax_total = $item_base_query->sum('tax_price');

            $invoice_details->save();

            // send notification(email & push)
            $invoice_message = tr('invoice_message');

            $invoice_message = str_replace("<%invoice_amount%>", formatted_amount($invoice_details->total),$invoice_message);

            $email_data['subject'] = Setting::get('site_name');

            $email_data['page'] = "emails.users.invoice-send";

            $email_data['data'] = $invoice_details;

            $email_data['invoice_items'] = $invoice_details ? \App\GeneratedInvoiceItem::where('generated_invoice_id',$invoice_details->id)->get() : [];

            $email_data['email'] = $invoice_details->to_user_email_address ?? '';

            $email_data['cc_user'] = $invoice_details->cc_user_email_address ?? '';

            $email_data['message'] = $invoice_message;

            $this->dispatch(new \App\Jobs\InvoiceJob($email_data));

            $invoice_details->save();

            DB::commit();

            return $this->sendResponse(api_success(121), 121, $invoice_details);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method user_disputes_index()
     * 
     * @uses get
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function user_disputes_index(Request $request) {

        try {

            $base_query = $total_query = \App\UserDispute::where('user_id', $request->id);

            $user_disputes = $base_query->skip($this->skip)->take($this->take)->get();

            $user_disputes = WalletRepo::disputes_list_response($user_disputes, $request);

            $data['user_disputes'] = $user_disputes;

            $data['total'] = $total_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method user_disputes_opened()
     * 
     * @uses get
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function user_disputes_opened(Request $request) {

        try {

            $base_query = $total_query = \App\UserDispute::where('status', DISPUTE_SENT)
                ->where('user_id', $request->id)
                ->orWhere(function ($query) use ($request) {
                    $query->where('receiver_user_id', $request->id)->where('status', DISPUTE_SENT);
                });

            $user_disputes = $base_query->skip($this->skip)->take($this->take)->get();

            $user_disputes = WalletRepo::disputes_list_response($user_disputes, $request);

            $data['user_disputes'] = $user_disputes;

            $data['total'] = $total_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method user_disputes_closed()
     * 
     * @uses get
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function user_disputes_closed(Request $request) {

        try {

            $base_query = $total_query = \App\UserDispute::where('user_id', $request->id)->orwhere('receiver_user_id', $request->id)->where('status', '!=', DISPUTE_SENT);

            $user_disputes = $base_query->skip($this->skip)->take($this->take)->get();

            $user_disputes = WalletRepo::disputes_list_response($user_disputes, $request);

            $data['user_disputes'] = $user_disputes;

            $data['total'] = $total_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method user_disputes_view()
     * 
     * @uses get
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function user_disputes_view(Request $request) {

        try {

            $user_dispute_details = \App\UserDispute::where('user_disputes.id', $request->user_dispute_id)->first();

            if(!$user_dispute_details) {

                throw new Exception(api_error(137), 137);
                
            }

            $cancel_btn_status = NO;

            if($request->id == $user_dispute_details->user_id) { // Who raised the dispute

                $user_details = $user_dispute_details->DisputeReceiver;

                // cancel button status

                $cancel_btn_status = $user_dispute_details->status == DISPUTE_SENT ? YES : NO;

                unset($user_dispute_details->DisputeReceiver);

            } else {

                $user_details = $user_dispute_details->DisputeSender;
                
                unset($user_dispute_details->DisputeSender);

            }

            $user_dispute_details->username = $user_details->name ?? "-";

            $user_dispute_details->user_picture = $user_details->picture ?? asset('placeholder.jpg');

            $user_dispute_details->cancel_btn_status = $cancel_btn_status;

            $user_dispute_details->chat_btn_status = 1;

            $user_dispute_details->chat_send_btn_status = in_array($user_dispute_details->status, [DISPUTE_SENT]) ? YES : NO;

            $data['user_dispute'] = $user_dispute_details;

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method user_disputes_send()
     * 
     * @uses send request to admin
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json with boolean output
     */

    public function user_disputes_send(Request $request) {

        try {
             
            DB::beginTransaction();

            // Validation start

            $rules = ['user_wallet_payment_id' => 'required|exists:user_wallet_payments,id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            // Check the user has enough balance 

            $user_wallet_payment = \App\UserWalletPayment::where('user_wallet_payments.id', $request->user_wallet_payment_id)->first();

            // dd($user_wallet_payment->toArray());

            if(!$user_wallet_payment) {
                throw new Exception(api_error(138), 138);
            }

            // Check the payment is eligible for dispute

            if($user_wallet_payment->status != USER_WALLET_PAYMENT_PAID && $user_wallet_payment->payment_type != WALLET_PAYMENT_TYPE_PAID) {
                throw new Exception(api_error(139), 139);
            }

            // Check any pending cases for wallet payment

            $check_user_disputes = \App\UserDispute::where('user_wallet_payment_id', $request->user_wallet_payment_id)->whereIn('status', [DISPUTE_SENT, DISPUTE_APPROVED])->count();

            if($check_user_disputes) {

                throw new Exception(api_error(141), 141);
                
            }
        
            $user_wallet_payment->status = USER_WALLET_PAYMENT_DISPUTED;

            $user_wallet_payment->save();

            $additional_inputs = ['user_id' => $request->id, 'receiver_user_id' => $user_wallet_payment->to_user_id, 'amount' => $user_wallet_payment->paid_amount];

            $request->request->add($additional_inputs);

            $user_dispute = \App\UserDispute::create($request->all());
            
            if($user_dispute) {

                PaymentRepo::user_wallet_update_dispute_send($user_dispute->amount, $user_wallet_payment->to_user_id);


                $from_user = \App\User::find($user_dispute->user_id);

                $to_user = \App\User::find($user_dispute->receiver_user_id);
               

                $dispute_sender = str_replace("<%user_name%>",$to_user->name??'',tr('user_dispute_from_description'));

                $dispute_sender = str_replace("<%reason%>",$user_dispute->message??'',$dispute_sender);

                $email_data['subject'] = Setting::get('site_name');

                $email_data['page'] = "emails.users.dispute-sender";

                $email_data['data'] = $user_dispute;

                $email_data['email'] = $from_user->email ?? '';

                $email_data['name'] = $from_user->name ?? '';

                $email_data['message'] = $dispute_sender;
               
                $this->dispatch(new \App\Jobs\SendEmailJob($email_data));


                $dispute_receiver = str_replace("<%user_name%>",$from_user->name??'',tr('user_dispute_to_description'));

                $dispute_receiver = str_replace("<%reason%>",$user_dispute->message??'',$dispute_receiver);
                
                $data['subject'] = Setting::get('site_name');
               
                $data['page'] = "emails.users.dispute-receiver";
                
                $data['data'] = $user_dispute;

                $data['email'] = $to_user->email ?? '';

                $data['name'] = $to_user->name ?? '';

                $data['message'] = $dispute_receiver;

                $this->dispatch(new \App\Jobs\SendEmailJob($data));

                DB::commit();

                $data['user_dispute'] = $user_dispute ?? [];

                return $this->sendResponse(api_success(125), 125, $data);

            }

            throw new Exception(api_error(140), 140);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method user_disputes_cancel()
     * 
     * @uses cancel request to admin
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json with boolean output
     */

    public function user_disputes_cancel(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
                    'user_dispute_id' => 'required|numeric|exists:user_disputes,id',
                    'cancel_reason' => 'nullable'
                ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            // Check the user has enough balance 

            $user_dispute_details = \App\UserDispute::where('user_id', $request->id)->where('user_disputes.id', $request->user_dispute_id)->first();

            if(!$user_dispute_details) {
                throw new Exception(api_error(137), 137);
            }

            // Check the cancel eligibility

            if($user_dispute_details->status != DISPUTE_SENT) {
                throw new Exception(api_error(142), 142);
            }

            $user_dispute_details->status = DISPUTE_CANCELLED;

            $user_dispute_details->cancel_reason = $request->cancel_reason ?: "";

            $user_dispute_details->save();

            // Update the wallet amount

            PaymentRepo::user_wallet_update_dispute_cancel($user_dispute_details->amount, $user_dispute_details->receiver_user_id);

            // Update the wallet history

            $user_wallet_payment = \App\UserWalletPayment::where('id', $user_dispute_details->user_wallet_payment_id)->first();

            if($user_wallet_payment) {

                $user_wallet_payment->status = USER_WALLET_PAYMENT_PAID;

                $user_wallet_payment->save();
            }

            DB::commit();

            $data['user_dispute'] = $user_dispute_details;

            return $this->sendResponse(api_success(126), 126, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method user_disputes_messages()
     * 
     * @uses
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json with boolean output
     */
    
    public function user_disputes_messages(Request $request) {

        try {

            $user_dispute_details = \App\UserDispute::where('user_disputes.id', $request->user_dispute_id)->first();

            if(!$user_dispute_details) {

                throw new Exception(api_error(137), 137);
                
            }

            $messages = \App\ResolutionCenter::where('user_dispute_id', $request->user_dispute_id)->get();

            foreach ($messages as $key => $message) {

                $message->message_align = $request->id == $message->sender_id ? 'sent' : 'receive';

                $message->username = $message->Sender->name ?? ($message->message_type == DISPUTE_ADMIN_TO_USER ? "Admin" : "");

                $message->user_picture = $message->Sender->picture ?? ($message->message_type == DISPUTE_ADMIN_TO_USER ? get_admin_picture() : asset('placeholder.jpg'));

                //$message->created_at = common_date($message->created_at, $this->timezone);

                unset($message->Sender);
                 
            }

            $data['user_dispute'] = $user_dispute_details;

            $data['messages'] = $messages ?? [];

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {
            
            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method user_disputes_messages_save()
     * 
     * @uses
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json with boolean output
     */
    
    public function user_disputes_messages_save(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
                    'user_dispute_id' => 'required|numeric|exists:user_disputes,id',
                    'message' => 'required'
                ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            // Check the user has enough balance 

            $user_dispute_details = \App\UserDispute::where('user_disputes.id', $request->user_dispute_id)->first();

            if(!$user_dispute_details) {
                throw new Exception(api_error(137), 137);
            }

            $resolution_center = new \App\ResolutionCenter;

            $message_type = $request->id == $user_dispute_details->user_id ?  DISPUTE_SENDER_TO_RECEIVER : DISPUTE_RECEIVER_TO_SENDER;

            $receiver_id = $request->id != $user_dispute_details->user_id ?: $user_dispute_details->receiver_user_id;  

            $resolution_center->user_dispute_id = $request->user_dispute_id;

            $resolution_center->user_wallet_payment_id = $user_dispute_details->user_wallet_payment_id;

            $resolution_center->sender_id = $request->id;

            $resolution_center->receiver_id = $receiver_id;

            $resolution_center->message = $request->message;

            $resolution_center->subject = $request->subject ?: "";

            $resolution_center->message_type = $message_type;

            $resolution_center->save();

            DB::commit();

            return $this->sendResponse($message = api_success(127), $code = 127, $resolution_center);

        } catch(Exception $e) {

            DB::rollback();
            
            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method invoices_business_information()
     * 
     * @uses
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json with boolean output
     */
    
    public function invoices_business_information(Request $request) {

        try {

            $generated_invoice = \App\GeneratedInvoice::where('sender_id', $request->id)->orderBy('created_at','DESC')->first();

            if($generated_invoice) {

                $info_details = \App\GeneratedInvoiceInfo::where('generated_invoice_id', $generated_invoice->id)->select('business_name', 'first_name', 'last_name', 'address', 'mobile', 'email', 'website', 'tax_number', 'additional_information')->orderBy('created_at','desc')->first();
            }


            return $this->sendResponse($message = "", $code = "", $info_details ?? []);

        } catch(Exception $e) {
            
            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method generated_invoices_payment()
     * 
     * @uses delete the selected iteem
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request
     *
     * @return json response
     */

    public function generated_invoices_payment(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['generated_invoice_id' => 'required|exists:generated_invoices,id'];

            $custom_errors = ['generated_invoice_id.exists' => api_error(129)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $invoice_details = \App\GeneratedInvoice::where('generated_invoices.id', $request->generated_invoice_id)->first();

            if(!$invoice_details) {

                throw new Exception(api_error(129), 129);
                
            }

            // Check the user have enough balance to pay this

            $user_wallet = \App\UserWallet::where('user_id', $request->id)->first();

            $remaining = $user_wallet->remaining ?? 0;

            if($remaining < $invoice_details->total) {

                throw new Exception(api_error(131), 131);
            }

            // Deduct the amount from to user wallet and add to sender wallet 

            // PaymentRepo::user_wallet_update_invoice_payment($invoice_details->total, $invoice_details->sender_id, $invoice_details->to_user_id);

            $request->request->add([
                'total' => $invoice_details->total, 
                'user_pay_amount' => $invoice_details->total,
                'paid_amount' => $invoice_details->total,
                'to_user_id' => $invoice_details->sender_id,
                'id' => $invoice_details->to_user_id,
                'payment_type' => WALLET_PAYMENT_TYPE_PAID,
                'amount_type' => WALLET_AMOUNT_TYPE_MINUS,
                'payment_id' => 'INDE-'.rand()
            ]);

            $payment_response = PaymentRepo::user_wallets_payment_save($request)->getData();

            if($payment_response->success ==  false) {
                
                throw new Exception($payment_response->error, $payment_response->error_code);
                
            }


            // Update the to user

            $to_user_inputs = [
                'to_user_id' => $invoice_details->to_user_id,
                'id' => $invoice_details->sender_id,
                'total' => $invoice_details->total, 
                'user_pay_amount' => $invoice_details->total,
                'paid_amount' => $invoice_details->total,
                'payment_type' => WALLET_PAYMENT_TYPE_CREDIT,
                'amount_type' => WALLET_AMOUNT_TYPE_ADD,
                'payment_id' => 'INCRE-'.rand()
            ];

            $to_user_request = new \Illuminate\Http\Request();

            $to_user_request->replace($to_user_inputs);

            $to_user_payment_response = PaymentRepo::user_wallets_payment_save($to_user_request)->getData();

            if($to_user_payment_response->success ==  false) {
                
                throw new Exception($to_user_payment_response->error, $to_user_payment_response->error_code);
                
            }


            $invoice_details->status = INVOICE_PAID;

            $invoice_details->user_wallet_payment_id = $payment_response->data ? $payment_response->data->user_wallet_payment_id : 0 ;

            $invoice_details->save();

            // @todo transactions update

            // @todo

            DB::commit();

            return $this->sendResponse($message = api_success(131), $code = 131, $info_details ?? []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

}
