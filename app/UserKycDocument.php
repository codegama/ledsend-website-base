<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserKycDocument extends Model
{
    protected $fillable = ['user_id', 'kyc_document_id','document_file'];

	protected $hidden = ['deleted_at', 'id', 'unique_id'];

	protected $appends = ['user_kyc_document_id','user_kyc_document_unique_id'];

    public function getUserKycDocumentIdAttribute() {

        return $this->id;
    }

    public function getUserKycDocumentUniqueIdAttribute() {

        return $this->unique_id;
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommonResponse($query) {

        return $query;
    
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = 'KID'."-".uniqid();

            $model->is_verified = USER_KYC_DOCUMENT_NONE;
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = 'KID'."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

        static::deleting(function ($model) {

        	\Helper::storage_delete_file($model->document_file, DOCUMENTS_PATH);

            \Helper::storage_delete_file($model->document_file_front, DOCUMENTS_PATH);

            \Helper::storage_delete_file($model->document_file_back, DOCUMENTS_PATH);

        });

    }

    public function documentDetails() {
    	return $this->belongsTo('App\KycDocument','kyc_document_id');
    }
}
