<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLookupsRelatedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(!Schema::hasTable('announcement_lists')) {

            Schema::create('announcement_lists', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->default(rand());
                $table->string('type')->comment('everyone | Merchant users | Personal Users');
                $table->string('subject');
                $table->string('message');
                $table->dateTime('published_date');
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
            
        }

        if(!Schema::hasTable('settings')) {

            Schema::create('settings', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('key');
                $table->text('value');
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
            
        }

        if(!Schema::hasTable('kyc_documents')) {

            Schema::create('kyc_documents', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->default(rand());
                $table->string('name');
                $table->string('image_type')->default('jpg');
                $table->string('picture')->default(asset('document.jpg'));
                $table->text('description')->nullable();
                $table->tinyInteger('document_type')->default(1)->comment('1 - normal user, 2 - merchant user, 3 - all users');
                $table->tinyInteger('is_required')->default(1);
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
            
        }

        if(!Schema::hasTable('static_pages')) {

            Schema::create('static_pages', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->default(uniqid());
                $table->string('title')->unique();
                $table->text('description');
                $table->enum('type',['about','privacy','terms','refund','cancellation','faq','help','contact','business','others'])->default('others');
                $table->string('section_type')->nullable();
                $table->tinyInteger('status')->default(APPROVED);
                $table->timestamps();
            });

        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kyc_documents');
        Schema::dropIfExists('settings');
        Schema::dropIfExists('static_pages');
        Schema::dropIfExists('announcement_lists');
    }
}
