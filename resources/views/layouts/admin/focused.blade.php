<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <title>{{Setting::get('site_name')}}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
        <link rel="icon" type="image/x-icon" href="{{Setting::get('site_icon')}}" />

        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-touch-fullscreen" content="yes" />

        <meta name="apple-mobile-web-app-status-bar-style" content="default" />

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

        <!-- Please remove the file below for production: Contains demo classes -->
        <link class="main-stylesheet" href="{{asset('admin-assets/css/style.css')}}" rel="stylesheet" type="text/css" />
     
        <link href="{{asset('admin-assets/plugins/toaster/toastr.min.css')}}" rel="stylesheet" />

     
        <style>
            .login-wrapper {
                background: url("{{asset('/images/login-bg.jpg')}}");
                background-size: contain;
                background-repeat: no-repeat;
                background-position: none;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
        </style>

    </head>
    <body class="fixed-header menu-pin menu-behind">
        <div class="login-wrapper">
            <!-- START Login Background Pic Wrapper-->
            <div class="bg-pic">
                <!-- START Background Caption-->
                <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
                </div>
                <!-- END Background Caption-->
            </div>
            <!-- END Login Background Pic Wrapper-->

            <!-- START Login Right Container-->

            @yield('content')
            <!-- END Login Right Container-->
            @include('layouts.admin.scripts')

            @include('notifications.notify')


        </div>
       

        <script src="{{asset('/admin-assets/js/jstz.min.js')}}"></script>

        <script>
            $(document).ready(function() {

                var dMin = new Date().getTimezoneOffset();
                var dtz = -(dMin/60);
                $("#admin-timezone").val(jstz.determine().name());
            });
        </script>
    </body>
</html>
