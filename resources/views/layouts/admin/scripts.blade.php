
<script src="{{asset('admin-assets/plugins/jquery/jquery.min.js')}}"></script>

<script src="{{asset('admin-assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<script src="{{asset('admin-assets/plugins/simplebar/simplebar.min.js')}}"></script>

<script src="https://unpkg.com/hotkeys-js/dist/hotkeys.min.js"></script>

<script src="{{asset('admin-assets/plugins/apexcharts/apexcharts.js')}}"></script>

<script src="{{asset('admin-assets/plugins/DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js')}}"></script>

<script src="{{asset('admin-assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.js')}}"></script>

<script src="{{asset('admin-assets/plugins/jvectormap/jquery-jvectormap-world-mill.js')}}"></script>

<script src="{{asset('admin-assets/plugins/nprogress/nprogress.js')}}"></script>

<script src="{{asset('admin-assets/plugins/jvectormap/jquery-jvectormap-us-aea.js')}}"></script>

<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>

<script src="{{asset('admin-assets/plugins/toaster/toastr.min.js')}}"></script>

<script src="{{asset('admin-assets/js/mono.js')}}"></script>

<script src="{{asset('admin-assets/js/chart.js')}}"></script>

<script src="{{asset('admin-assets/js/map.js')}}"></script>

<script src="{{asset('admin-assets/js/select2.min.js')}}"></script>

<script src="{{asset('admin-assets/js/custom.js')}}"></script>


<script src="{{asset('admin-assets/plugins/daterangepicker/moment.min.js')}}"></script>

<script src="{{asset('admin-assets/plugins/daterangepicker/daterangepicker.js')}}"></script>

<!-- <script src="http://cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script> -->
<script>
    $(document).ready(function() {

    	// if($('.ckeditor').length) {
    	// 	CKEDITOR.replace('ckeditor');
    	// }
        
        $('.select2').select2();
        
        var dataTable = $('#dataTable');
		 
		if (dataTable.length != 0){

		    dataTable.DataTable({
            	info: false,
	            "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
				"pageLength":10,
				"lengthChange": false,
				'sDom': 'lrtip',
		    });
		}

		$('#dataTable_paginate').hide();
    
    });


	
</script>



