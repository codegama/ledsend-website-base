@extends('layouts.admin')

@section('page_header',tr('users'))

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.users.index')}}">{{tr('users')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('view_users')}}</li>

@endsection

@section('content')


<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('view_users')}}

            <button type="button" class="badge badge-square badge-outline-light" data-toggle="popover" data-content="{{tr('users_note')}}">?</button>

            <a class="btn btn-white pull-right" href="{{route('admin.users.create')}}">
                <i class="fa fa-plus"></i> {{tr('add_user')}}
            </a>

                 @include('admin.users.bulk_actions')
        </h4>

        

       
    </div>

   


    <div class="card-body">

        @include('admin.users._search')

        <div class="table-responsive">

            <table id="dataTable" class="table data-table">

                <thead>
                    <tr>
                    <th>
                        <input id="check_all" type="checkbox" class="chk-box-left">
                    </th>
                        <th>{{tr('s_no')}}</th>
                        <th>{{tr('name')}}</th>
                        <th>{{tr('email')}}</th>
                        <th>{{tr('wallets')}}</th>
                        <th>{{tr('status')}}</th>
                        <th>{{tr('kyc_status')}}</th>
                        <th>{{tr('action')}}</th>
                    </tr>
                
                </thead>

                <tbody>

                    @foreach($users as $i => $user_details)

                    <tr>
                    <td id="check{{$user_details->id}}"><input type="checkbox" name="row_check" class="faChkRnd chk-box-inner-left" id="{{$user_details->id}}" value="{{$user_details->id}}"></td>
                        <td>{{$i+$users->firstItem()}}</td>

                        <td>
                            <a href="{{route('admin.users.view', ['user_id' => $user_details->id])}}">{{$user_details->name}}
                            </a>

                        </td>

                        <td>
                            {{$user_details->email}}
                            <span><h6>{{$user_details->mobile?: tr('not_available')}}</h6></span>
                        </td>

                        <td>
                            <a class="btn btn-outline-warning btn-sm" href="{{route('admin.user_wallets.view', ['user_id' => $user_details->id])}}">
                                {{tr('wallets')}}
                            </a>

                        </td>

                        <td>
                            @if($user_details->status == YES)

                            <span class="text-success">{{tr('approved')}}</span>

                            @else

                            <span class="text-danger">{{tr('declined')}}</span>

                            @endif
                        </td>

                        <td>

                            @if($user_details->is_kyc_document_approved == USER_KYC_DOCUMENT_APPROVED)

                                <span class="text-success">{{tr('verified')}}</span>

                            @else

                                <a class="btn btn-outline-secondary btn-sm" href="{{route('admin.user_kyc_documents.view', ['user_id' => $user_details->id])}}">
                                    {{tr('verify')}}
                                </a>

                            @endif

                        </td>

                        <td>@include('admin.users._actions')</td>

                    </tr>

                    @endforeach

                </tbody>

            </table>

            <div class="pull-right">{{$users->appends(request()->input())->links()}}</div>
        </div>

    </div>

</div>

@endsection

@include('admin.users.bulk_action_scripts')