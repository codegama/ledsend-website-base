<div class="card-body">

    @if(Setting::get('is_demo_control_enabled') == NO )

    <form method="POST" action="{{route('admin.users.save')}}" enctype="multipart/form-data" role="form">

    @else

    <form class="forms-sample" role="form">

    @endif

        @csrf

        <div class="form-body">

            <h3 class="card-title">{{tr('user_info')}}</h3>

            <hr>

            <input type="hidden" name="user_id" value="{{$user_details->id}}">

            <div class="row p-t-20">

                <div class="form-group col-md-6">

                    <label class="control-label">{{tr('name')}}<span class="admin-required">*</span></label>
                    <input type="text" name="name" value="{{old('name') ?: $user_details->name}}" class="form-control" placeholder="{{tr('name')}}" required>

                </div>

                <div class="form-group col-md-6">

                    <label class="control-label">{{tr('email')}}<span class="admin-required">*</span></label>
                    <input type="email" name="email" value="{{old('email') ?: $user_details->email}}" class="form-control" placeholder="{{tr('email')}}" required>

                </div>

                @if($user_details->id == null)

                <div class="form-group col-md-6">

                    <label class="control-label">{{tr('password')}}<span class="admin-required">*</span></label>
                    <input type="password" name="password" value="{{old('password')}}" class="form-control" placeholder="{{tr('password')}}" required>

                </div>

                <div class="form-group col-md-6">

                    <label class="control-label">{{tr('confirm_password')}}<span class="admin-required">*</span></label>
                    <input type="password" class="form-control" name="password_confirmation" placeholder="{{tr('confirm_password')}}" value="{{old('confirm_password')}}" required>

                </div>

                @endif

                <div class="form-group col-md-6">

                    <label class="control-label">{{tr('mobile')}}<span class="admin-required">*</span></label>
                    <input type="number" min="1" step="any" class="form-control" name="mobile" value="{{old('mobile') ?: $user_details->mobile}}" placeholder="867676576" required>

                </div>

                <div class="form-group col-lg-6 col-md-6">

                    <label for="input-file-now">{{tr('picture')}}</label>
                    <span class="file_input_format_css"> {{tr('upload_files')}}</span>
                    <input type="file" id="input-file-now" name="picture" class="form-control dropify" accept="image/*" />

                </div>

                <div class="form-group col-md-6">

                    <label class="control-label">{{tr('gender')}}<span class="admin-required">*</span></label>
                    <select class="form-control" name="gender" required>
                        <option value="">{{tr('select')}}</option>
                        <option value="MALE" @if($user_details->gender == MALE) selected @endif>{{tr('male')}}</option>
                        <option value="FEMALE" @if($user_details->gender == FEMALE) selected @endif>{{tr('female')}}</option>

                    </select>

                </div>

            </div>



            <div class="row">

                <div class="form-group col-md-12">

                    <label>{{tr('about')}}</label>

                    <textarea class="form-control" name="about">{{old('about') ?? ($user_details->about ?: "")}}</textarea>

                </div>
                <input type="hidden" name="timezone" value="" id="userTimezone">


            </div>

        </div>

        <div class="form-actions">

            @if(Setting::get('is_demo_control_enabled') == NO )

            <button type="submit" class="btn btn-primary btn-pill pull-right"> <i class="fa fa-check"></i>{{tr('save')}}</button>

            @else

            <button type="button" class="btn btn-primary btn-pill pull-right"> <i class="fa fa-check"></i>{{tr('save')}}</button>

            @endif

            <a  href="{{route('admin.users.index')}}" class="btn reset-btn btn-pill">{{tr('reset')}}</a>

        </div>

    </form>

</div>