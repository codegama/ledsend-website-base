@extends('layouts.admin')

@section('page_header',tr('users'))

@section('styles')

<link rel="stylesheet" href="{{asset('admin-assets/css/dropify.min.css')}}">

<link href="{{asset('admin-assets/css/datepicker.css')}}" rel="stylesheet">

@endsection

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.users.index')}}">{{tr('users')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)" aria-current="page"></a><span>{{tr('add_user')}}</span></li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('add_user')}}

            <a class="btn btn-white pull-right" href="{{route('admin.users.index')}}">
                <i class="fa fa-eye"></i> {{tr('view_users')}}
            </a>
        </h4>

    </div>

    @include('admin.users._form')

</div>

@endsection

<script src="{{asset('admin-assets/js/jstz.min.js')}}"></script>

@section('scripts')

<script>
    jQuery(document).ready(function() {
        jQuery('input[name="dob"]').daterangepicker({
            autoUpdateInput: false,
            singleDatePicker: true,
            locale: {
                cancelLabel: 'Clear',
                format: 'MM/DD/YYYY'
            }
        });
        jQuery('input[name="dob"]').on('apply.daterangepicker', function(ev, picker) {
            jQuery(this).val(picker.startDate.format('MM/DD/YYYY'));
        });
        jQuery('input[name="dob"]').on('cancel.daterangepicker', function(ev, picker) {
            jQuery(this).val('');
        });
    });

    $(document).ready(function() {

        var dMin = new Date().getTimezoneOffset();
        var dtz = -(dMin / 60);
        // alert(dtz);
        $("#userTimezone").val(jstz.determine().name());
    });
</script>
@endsection



