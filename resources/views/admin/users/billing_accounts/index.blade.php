@extends('layouts.admin')

@section('page_header',tr('users'))

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.users.index')}}">{{tr('view_users')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('billing_accounts')}}</li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">


        <h4 class="m-b-0 text-white"><a class="m-b-0 text-white" href="{{route('admin.users.view',['user_id'=>$user_details->id])}}">{{$user_details->name??''}}</a> - {{ tr('billing_accounts')}}

            <button type="button" class="badge badge-square badge-outline-light" data-toggle="popover" data-content="{{tr('billing_accounts_note')}}">?</button>

            <a class="btn btn-white pull-right" href="{{route('admin.user_billing_accounts.create',['user_id'=>$user_details->id])}}">
                <i class="fa fa-plus"></i> {{tr('add_billing_accounts')}}
            </a>
        </h4>

    </div>

    <div class="card-body">

        @include('admin.users.billing_accounts._search')

        <div class="table-responsive">


            <table id="dataTable" class="table data-table">

                <thead>
                    <tr>
                        <th>{{tr('s_no')}}</th>
                        <th>{{tr('account_holder_name')}}</th>
                        <th>{{tr('account_no')}}</th>
                        <th>{{tr('ifsc_code')}}</th>
                        <th>{{tr('swift_code')}}</th>
                        <th>{{tr('action')}}</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach($user_billing_accounts as $i => $account_details)

                    <tr>
                        <td>{{$i+$user_billing_accounts->firstItem()}}</td>

                        <td>
                            <a href="{{route('admin.user_billing_accounts.view',['billing_account_id'=>$account_details->id])}}"> {{$account_details->account_holder_name}}</a>
                        </td>

                        <td>
                            {{$account_details->account_number}}
                        </td>

                        <td>{{$account_details->ifsc_code}}</td>

                        <td>
                            {{$account_details->swift_code}}
                        </td>



                        <td >
                            <a class="btn btn-outline-info btn-sm" href="{{ route('admin.user_billing_accounts.edit', ['billing_account_id' => $account_details->id]) }}">
                                {{ tr('edit') }}
                            </a>&nbsp;

                            <a class="btn btn-outline-danger btn-sm" href="{{ route('admin.user_billing_accounts.delete', ['billing_account_id' => $account_details->id]) }}" onclick="return confirm(&quot;{{tr('account_delete_confirmation')}}&quot;);">
                                {{ tr('delete') }}
                            </a>

                        </td>



                    </tr>
                    @endforeach

                </tbody>

            </table>

            <div class="pull-right">{{ $user_billing_accounts->appends(request()->input())->links() }}</div>
        </div>

    </div>

</div>

@endsection