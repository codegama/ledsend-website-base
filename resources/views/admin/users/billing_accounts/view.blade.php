@extends('layouts.admin')

@section('page_header',tr('users'))

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.users.index')}}">{{tr('view_users')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('view_billing_accounts')}}</li>
@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('view_billing_accounts')}}</h4>

    </div>

    <div class="card-body">

        <div class="row">

            <div class="col-6">

                <!-- Card content -->
                <div class="card-body">

                    <div class="template-demo">

                        <table class="table mb-0">

                            <thead>

                            </thead>

                            <tbody>

                                <tr>
                                    <td class="pl-0"><b>{{ tr('name') }}</b></td>
                                    <td class="pr-0 text-right">
                                        <div><a href="{{route('admin.users.view',['user_id'=>$user_account_details->user_id])}}">{{$user_account_details->user->name}}</a></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="pl-0"><b>{{ tr('nickname') }}</b></td>
                                    <td class="pr-0 text-right">
                                        <div>{{$user_account_details->nickname}}</div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="pl-0"><b>{{ tr('account_holder_name') }}</b></td>
                                    <td class="pr-0 text-right">
                                        <div>{{$user_account_details->account_holder_name}}</div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="pl-0"><b>{{ tr('account_no') }}</b></td>
                                    <td class="pr-0 text-right">
                                        <div>{{$user_account_details->account_number ?:tr('not_available')}}</div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="pl-0"><b>{{ tr('bank_name') }}</b></td>
                                    <td class="pr-0 text-right">
                                        <div>{{$user_account_details->bank_name ?:tr('not_available')}}</div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="pl-0"><b>{{ tr('ifsc_code') }}</b></td>
                                    <td class="pr-0 text-right">
                                        <div>{{$user_account_details->ifsc_code}}</div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="pl-0"><b>{{ tr('swift_code') }}</b></td>
                                    <td class="pr-0 text-right">
                                        <div>{{$user_account_details->swift_code}}</div>
                                    </td>
                                </tr>



                                <tr>

                                    <td class="pl-0"> <b>{{ tr('status') }}</b></td>

                                    <td class="pr-0 text-right">

                                        @if($user_account_details->status == APPROVED)

                                              <span class="card-text badge badge-success badge-md text-uppercase">{{tr('approved')}}</span>

                                        @else

                                               <span class="card-text badge badge-danger badge-md text-uppercase">{{tr('declined')}}</span>

                                        @endif

                                    </td>

                                </tr>



                                <tr>
                                    <td class="pl-0"> <b>{{ tr('created_at') }}</b></td>
                                    <td class="pr-0 text-right">
                                        <div>{{ common_date($user_account_details->created_at, Auth::guard('admin')->user()->timezone,'d M Y H:i:s') }}</div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="pl-0"> <b>{{ tr('updated_at') }}</b></td>
                                    <td class="pr-0 text-right">
                                        <div>{{ common_date($user_account_details->updated_at,Auth::guard('admin')->user()->timezone, 'd M Y H:i:s') }}</div>
                                    </td>
                                </tr>

                            </tbody>

                        </table>

                    </div>

                </div>

            </div>


            <div class="col-6">

                <br><br>

                <div class="row">

                    <div class="col-6">

                        @if(Setting::get('is_demo_control_enabled') == YES)

                        <a href="javascript:;" class="btn btn-primary btn-block">{{tr('edit')}}</a>

                        <a href="javascript:;" class="btn btn-danger btn-block">{{tr('delete')}}</a>

                        @else

                        <a class="btn btn-primary btn-block" href="{{ route('admin.user_billing_accounts.edit', ['billing_account_id' => $user_account_details->id])}}">{{tr('edit')}}</a>

                        <a class="btn btn-danger btn-block" href="{{route('admin.user_billing_accounts.delete', ['billing_account_id' => $user_account_details->id])}}" onclick="return confirm(&quot;{{tr('account_delete_confirmation' , $user_account_details->name)}}&quot;);">{{tr('delete')}}</a>

                        @endif

                    </div>



                </div>



            </div>

        </div>

    </div>

</div>

@endsection