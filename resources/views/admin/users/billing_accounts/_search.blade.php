<div class="col-md-12 mb-2">

    <form class="col-sm-12 col-sm-offset-6 search_box_css" action="{{route('admin.user_billing_accounts.index')}}" method="GET" role="search">
        {{csrf_field()}}

        <div class="row input-group">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <input type="hidden" name="user_id" value="{{$user_details->id??''}}">
                <input type="text" class="form-control" name="search_key" placeholder="{{tr('billing_account_search_placeholder')}}" value="{{Request::get('search_key')??''}}">
            </div>

            <div class="col-md-3 flex">
                <span class="input-group-btn">
                    &nbsp; &nbsp;
                    <button type="submit" class="btn btn-info">
                        <span class="glyphicon glyphicon-search"> {{tr('search')}}</span>
                    </button>
                    <a class="btn btn-danger" href="{{route('admin.user_billing_accounts.index',['user_id'=>$user_details->id])}}">{{tr('clear')}}</a>
                </span>
            </div>
        </div>

    </form>

</div>