<div class="col-md-12 mb-2">


        <form class="col-sm-12 col-sm-offset-6 search_box_css" action="{{route('admin.user_wallet_payments.index')}}" method="GET" role="search">


            {{csrf_field()}}

            <div class="row input-group">
                <div class="col-md-3">
                    <input type="text" class="form-control" name="search_key" placeholder="{{tr('wallet_payment_search_placeholder')}}" value="{{Request::get('search_key')??''}}"> <span class="input-group-btn">
                </div>

                <div class="col-md-3">

                    <select class="form-control" name="payment_amount_type">

                        <option value="">{{tr('select_amount_type')}}</option>

                        <option value="{{WALLET_PAYMENT_TYPE_ADD}}" @if(Request::get('payment_amount_type')==WALLET_PAYMENT_TYPE_ADD) selected @endif>{{tr('add')}}</option>

                        <option value="{{WALLET_PAYMENT_TYPE_PAID}}" @if(Request::get('payment_amount_type')==WALLET_PAYMENT_TYPE_PAID) selected @endif>{{tr('paid')}}</option>

                        <option value="{{WALLET_PAYMENT_TYPE_CREDIT}}" @if(Request::get('payment_amount_type')==WALLET_PAYMENT_TYPE_CREDIT) selected @endif>{{tr('credit')}}</option>

                        <option value="{{WALLET_PAYMENT_TYPE_WITHDRAWAL}}" @if(Request::get('payment_amount_type')==WALLET_PAYMENT_TYPE_WITHDRAWAL) selected @endif>{{tr('withdrawal')}}</option>

                    </select>
                </div>

                <div class="col-md-3">

                    <select class="form-control" name="payment_mode" class="text-uppercase">

                        <option value="">{{tr('select_payment_mode')}}</option>
                        <option value="{{COD}}" @if(Request::get('payment_mode')== COD) selected @endif >{{tr('cod')}}</option>
                        <option value="{{CARD}}" @if(Request::get('payment_mode')== CARD) selected @endif>{{tr('card')}}</option>
                        <option value="{{BANK_TRANSFER}}" @if(Request::get('payment_mode')== BANK_TRANSFER)selected @endif>{{tr('bank_transfer')}}</option>
                        <option value="{{PAYMENT_OFFLINE}}" @if(Request::get('payment_mode')== PAYMENT_OFFLINE) selected @endif>{{tr('offline')}}</option>
                        <option value="{{PAYMENT_MODE_WALLET}}" @if(Request::get('payment_mode')== PAYMENT_MODE_WALLET) selected @endif>{{tr('wallet')}}</option>

                    </select>
                </div>

                @if(Request::get('type'))
                    <input type="hidden" name="type" value="{{BANK_TRANSFER}}">
                @endif

                <div class="col-md-3 flex">

                    <button type="submit" class="btn btn-info m-r-1">
                        <span class="glyphicon glyphicon-search"> {{tr('search')}}</span>
                    </button>

                    @if(Request::get('type'))
                        <a class="btn btn-danger" href="{{route('admin.user_wallet_payments.index',['type'=>BANK_TRANSFER])}}">{{tr('clear')}}</a>
                    @else
                        <a class="btn btn-danger" href="{{route('admin.user_wallet_payments.index')}}">{{tr('clear')}}</a>
                    @endif
                
                </div>
            </div>

        </form>
</div>