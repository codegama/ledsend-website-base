<div class="col-md-12 mb-2">

    <form class="col-sm-12 col-sm-offset-6 search_box_css" action="{{route('admin.user_wallets.index')}}" method="GET" role="search">

        <div class="row input-group">
            
            <div class="col-md-5"></div>
            
            <div class="col-md-4">
               <input type="text" class="form-control" name="search_key" placeholder="{{tr('user_search_placeholder')}}" value="{{Request::get('search_key')??''}}">
            </div>  
            
            <div class="col-md-3 flex">

                <span class="input-group-btn">
                    &nbsp; &nbsp;
                      
                    <button type="submit" class="btn btn-info">

                        @if(Request::get('today_wallet'))
                        <input type="hidden" name="today_wallet" value="{{Request::get('today_wallet')??''}}">
                        @endif

                        <span class="glyphicon glyphicon-search"> {{tr('search')}}</span>
                    </button>

                    <a class="btn btn-danger" href="{{route('admin.user_wallets.index')}}">{{tr('clear')}}</a>
                </span>
             </div>
                
        </div>

    </form>

</div>