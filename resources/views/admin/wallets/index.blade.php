@extends('layouts.admin')

@section('page_header',tr('user_wallets'))

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.users.index')}}">{{tr('wallets')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('user_wallets')}}</li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('user_wallets')}}

            <button type="button" class="badge badge-square badge-outline-light" data-toggle="popover" data-content="{{tr('user_wallets_note')}}">?</button>
            <a class="btn btn-white pull-right" href="{{route('admin.user_wallet_payments.index')}}">
                <i class="fa fa-plus"></i> {{tr('view_transactions')}}
            </a>

        </h4>

    </div>

    <div class="card-body">

        @include('admin.wallets._search')

        <div class="table-responsive">

            <table id="dataTable" class="table data-table">

                <thead>
                    <tr>
                        <th>{{ tr('s_no') }}</th>
                        @if(!Request::get('user_id'))
                        <th>{{ tr('name') }}</th>
                        @endif
                        <th>{{tr('email')}}</th>
                        <th>{{ tr('total') }}</th>
                        <th>{{ tr('remaining') }}</th>
                        <th>{{ tr('used') }}</th>
                        <th>{{ tr('updated_at') }}</th>
                        <th>{{ tr('action') }}</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach($user_wallets as $i => $user_wallet_details)

                    <tr>

                        <td>{{$i+$user_wallets->firstItem()}}</td>

                        @if(!Request::get('user_id'))
                            <td>
                                <a href="{{route('admin.users.view' , ['user_id' => $user_wallet_details->user_id])}}"> {{ $user_wallet_details->name ?? tr('user_not_avail')}}
                               </a>
                            </td>
                        @endif

                        <td>
                            {{$user_wallet_details->email ?? '' }}
                            <p class="text-muted">{{$user_wallet_details->mobile ?? ''}}<p>
                        </td>


                        <td>{{$user_wallet_details->total_formatted}}</td>

                        <td class="text-success">{{$user_wallet_details->remaining_formatted}}</td>

                        <td>{{$user_wallet_details->used_formatted}}</td>

                        <td>
                            {{ common_date($user_wallet_details->updated_at, Auth::guard('admin')->user()->timezone) }}
                        </td>

                        <td>
                            <a class="btn btn-outline-warning btn-sm" href="{{route('admin.user_wallets.view', ['user_id' => $user_wallet_details->user_id])}}">
                                {{tr('view')}}
                            </a>

                        </td>

                    </tr>

                    @endforeach

                </tbody>

            </table>

            <div class="pull-right">{{$user_wallets->appends(request()->query())->links()}}</div>

        </div>

    </div>

</div>

@endsection