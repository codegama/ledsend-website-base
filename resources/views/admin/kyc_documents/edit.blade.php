@extends('layouts.admin')

@section('title', tr('edit_document'))

@section('breadcrumb')
	
    <li class="breadcrumb-item"><a href="{{ route('admin.kyc_documents.index') }}">{{tr('documents')}}</a></li>
    <li class="breadcrumb-item active" aria-current="page">
    	<span>{{ tr('edit_document') }}</span>
    </li>
           
@endsection 

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('edit_document')}}

            <a class="btn btn-white pull-right" href="{{route('admin.kyc_documents.index')}}">
                <i class="fa fa-eye"></i> {{tr('view_document')}}
            </a>
        </h4>

    </div>

    @include('admin.kyc_documents._form')

</div>

@endsection