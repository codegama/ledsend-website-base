@extends('layouts.admin')

@section('title', tr('documents'))

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{ route('admin.kyc_documents.index') }}">{{tr('documents')}}</a></li>

<li class="breadcrumb-item active" aria-current="page">
    <span>{{ tr('view_documents') }}</span>
</li>

@endsection

@section('content')

<div class="col-lg-12 grid-margin stretch-card">

    <div class="card">

        <div class="card-header bg-info">

            <h4 class="m-b-0 text-white">{{tr('user_documents')}}
            <button type="button" class="badge badge-square badge-outline-light" data-toggle="popover" data-content="{{tr('kyc_documents_note')}}">?</button>

                <a class="btn btn-white pull-right" href="{{route('admin.kyc_documents.create')}}">
                    <i class="fa fa-plus"></i> {{tr('add_document')}}
                </a>
            </h4>

        </div>

        <div class="card-body">

            @include('admin.kyc_documents._search')

            <div class="table-responsive">

                <table id="dataTable" class="table data-table">

                    <thead>
                        <tr>
                            <th>{{tr('s_no')}}</th>
                            <th>{{tr('document_name')}}</th>
                            <th>{{tr('status')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>
                    </thead>

                    <tbody>

                        @foreach($kyc_documents as $i => $document_details)

                        <tr>
                            <td>{{$i+1}}</td>

                            <td>
                                <a href="{{route('admin.kyc_documents.view' , ['kyc_document_id' => $document_details->id] )}}"> {{$document_details->name}}
                                </a>
                            </td>

                            <td>
                                @if($document_details->status == APPROVED)

                                <span class="badge badge-outline-success">
                                    {{ tr('approved') }}
                                </span>

                                @else

                                <span class="badge badge-outline-danger">
                                    {{ tr('declined') }}
                                </span>

                                @endif
                            </td>

                            <td>

                                <div class="template-demo">

                                    <div class="dropdown">

                                        <button class="btn btn-outline-primary  dropdown-toggle btm-sm" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{tr('action')}}
                                        </button>

                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">

                                            <a class="dropdown-item" href="{{ route('admin.kyc_documents.view',['kyc_document_id' => $document_details->id]) }}">
                                                {{tr('view')}}
                                            </a>

                                            @if(Setting::get('is_demo_control_enabled') == NO)

                                            <a class="dropdown-item" href="{{ route('admin.kyc_documents.edit',['kyc_document_id' => $document_details->id]) }}">
                                                {{tr('edit')}}
                                            </a>

                                            <a class="dropdown-item" onclick="return confirm(&quot;{{tr('document_delete_confirmation' , $document_details->name)}}&quot;);" href="{{ route('admin.kyc_documents.delete',['kyc_document_id' => $document_details->id]) }}">
                                                {{ tr('delete') }}
                                            </a>

                                            @else

                                            <a class="dropdown-item text-muted" href="javascript:;">
                                                {{tr('edit')}}
                                            </a>

                                            <a class="dropdown-item text-muted" href="javascript:;">
                                                {{ tr('delete') }}
                                            </a>

                                            @endif

                                            <div class="dropdown-divider"></div>

                                            @if($document_details->status == APPROVED)

                                            <a class="dropdown-item" href="{{ route('admin.kyc_documents.status',['kyc_document_id' => $document_details->id]) }}" onclick="return confirm(&quot;{{ $document_details->name }}-{{tr('document_decline_confirmation' , $document_details->name)}}&quot;);">

                                                {{tr('decline')}}
                                            </a>

                                            @else

                                            <a class="dropdown-item" href="{{ route('admin.kyc_documents.status',['kyc_document_id' => $document_details->id]) }}">
                                                {{tr('approve')}}
                                            </a>

                                            @endif


                                        </div>

                                    </div>

                                </div>

                            </td>

                        </tr>

                        @endforeach

                    </tbody>

                </table>

            </div>

        </div>

    </div>

</div>

@endsection