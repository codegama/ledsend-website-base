@extends('layouts.admin')

@section('page_header',tr('documents'))

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.generated_invoices.index')}}">{{tr('invoices')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{ Request::get('pending_invoices')?tr('pending_invoices'):tr('view_invoices')}}</li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">

            <a class="m-b-0 text-white" href="javascript:void(0)"> {{ Request::get('pending_invoices')?tr('pending_invoices'):tr('view_invoices')}}</a>
            <button type="button" class="badge badge-square badge-outline-light" data-toggle="popover" data-content="{{Request::get('pending_invoices')?tr('pending_invoices_note'):tr('invoices_note')}}">?</button>

            @if(Request::get('pending_invoices'))
            <a class="btn btn-white pull-right" href="{{route('admin.generated_invoices.index')}}">
                <i class="fa fa-plus"></i> {{tr('view_invoices')}}
            </a>
            @else
            <a class="btn btn-white pull-right" href="{{route('admin.generated_invoices.index',['pending_invoices'=>YES])}}">
                <i class="fa fa-plus"></i> {{tr('pending_invoices')}}
            </a>

            @endif
        </h4>

    </div>

    <div class="card-body ">

        @include('admin.invoices._search')
        <div class="table-responsive">

            <table id="dataTable" class="table data-table">

                <thead>
                    <tr>
                        <th>{{ tr('s_no') }}</th>
                        <th>{{ tr('sender') }}</th>
                        <th>{{ tr('receiver') }}</th>
                        <th>{{ tr('invoice_no') }}</th>
                        <th>{{ tr('invoice_date') }}</th>
                        <th>{{ tr('due_date') }}</th>
                        <th>{{ tr('invoice_status') }}</th>
                        <th>{{ tr('payment_status') }}</th>
                        <th>{{ tr('action') }}</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach($generated_invoices as $index => $invoice_details)

                    <tr>

                        <td>{{ $index+$generated_invoices->firstItem() }}</td>

                        <td>
                            <a href="{{route('admin.generated_invoices.view',['generated_invoice_id'=>$invoice_details->id])}}"> {{$invoice_details->from_user->name??''}}</a>
                        </td>

                        <td>
                            <a href="{{route('admin.generated_invoices.view',['generated_invoice_id'=>$invoice_details->id])}}"> {{$invoice_details->to_user->name?? tr('not_assigned')}}</a>
                        </td>


                        <td>
                            {{ $invoice_details->invoice_number ?: tr('not_available')}}
                        </td>

                        <td>

                            {{ common_date($invoice_details->invoice_date,Auth::guard('admin')->user()->timezone,'d M Y') }}
                        </td>

                        <td>

                           {{ common_date($invoice_details->due_date,Auth::guard('admin')->user()->timezone,'d M Y') }}
                         </td>

                        <td>
                            @if($invoice_details->status == INVOICE_SENT)

                            {{tr('sent')}}
                            @elseif($invoice_details->status == INVOICE_SCHEDULED)

                            {{tr('scheduled')}}
                            @elseif($invoice_details->status == INVOICE_PAID)

                            {{tr('paid')}}
                            @elseif($invoice_details->status == INVOICE_DRAFT)

                            {{tr('draft')}}
                            @endif

                        </td>

                        <td>

                            @if($invoice_details->paid_status == UNPAID)

                            {{tr('unpaid')}}

                            @elseif($invoice_details->paid_status == INVOICE_SCHEDULED)

                            {{tr('waiting_for_payment')}}

                            @elseif($invoice_details->paid_status == INVOICE_SENT)

                            {{tr('paid')}}

                            @elseif($invoice_details->paid_status == INVOICE_PAID)

                            {{tr('expired')}}

                            @else
                            {{ tr('cancelled')}}

                            @endif

                        </td>

                        <td class="flex">
                            <a class="btn btn-outline-info btn-sm" target="_blank" href="{{ route('admin.generated_invoices.view', ['generated_invoice_id' => $invoice_details->id]) }}">
                                {{ tr('view') }}
                            </a> &nbsp;&nbsp;

                            <a class="btn btn-outline-danger btn-sm" href="{{ route('admin.generated_invoices.delete', ['generated_invoice_id' => $invoice_details->id]) }}" onclick="return confirm(&quot;{{tr('invoice_delete_confirmation')}}&quot;);">
                                {{ tr('delete') }}
                            </a>

                        </td>

                    </tr>

                    @endforeach

            </table>

            <div class="pull-right"> {{ $generated_invoices->appends(request()->input())->links() }} </div>
        </div>

    </div>

</div>
@endsection