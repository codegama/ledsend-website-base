@extends('layouts.admin')

@section('page_header',tr('documents'))

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.generated_invoices.index')}}">{{tr('invoices')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('view_invoices')}}</li>

@endsection

@section('content')


<div class="invoice-wrapper rounded border bg-white py-5 px-3 px-md-4 px-lg-5 mb-6">
    <div class="d-flex justify-content-between">
        <h2 class="text-dark font-weight-medium">{{tr('invoice')}} #{{$invoice_details->invoice_number??'-'}}</h2>
        <div class="btn-group">
            <!-- <button class="btn btn-sm btn-light">
                <i class="mdi mdi-content-save"></i> {{tr('save')}}</button> -->
            <button class="btn btn-sm btn-warning" onclick="window.print()">
                <i class="mdi mdi-printer"></i> {{tr('print')}}</button>
        </div>
    </div>
    <div class="row pt-5">
        <div class="col-xl-3 col-lg-4">
            <p class="text-dark mb-2">{{tr('from')}}</p>
            <address>
                {{$invoice_info->business_name??'-'}}
                <br> {{$invoice_details->from_user->name??'-'}}
                <br> {{tr('email')}}: {{$invoice_details->from_user->email??'-'}}
                <br> {{tr('mobile')}}: {{$invoice_details->from_user->mobile??'-'}}
            </address>
        </div>
        <div class="col-xl-3 col-lg-4">
            <p class="text-dark mb-2">{{tr('to')}}</p>
            <address>
                <br> {{$invoice_details->to_user->name??tr('not_assigned')}}
                <br> {{tr('email')}}: {{$invoice_details->to_user->email?? tr('not_assigned')}}
                <br> {{tr('mobile')}}: {{$invoice_details->to_user->mobile??tr('not_assigned')}}
            </address>
        </div>
        <div class="col-xl-3 col-lg-4">
            <p class="text-dark mb-2">{{tr('details')}}</p>
            <address>
                {{tr('invoice_id')}}:
                <span class="text-dark">#{{$invoice_details->invoice_number??'-'}}</span>
                <br> <p>{{tr('invoice_date')}}:{{common_date($invoice_details->invoice_date,Auth::guard('admin')->user()->timezone,'d M Y')}}</p>
                <p>{{tr('due_date')}} :{{common_date($invoice_details->due_date,Auth::guard('admin')->user()->timezone,'d M Y')}}</p> 
            </address>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table mt-3 table-striped" style="width:100%">
            <thead>
                <tr>
                    <th>{{tr('s_no')}}</th>
                    <th>{{tr('item')}}</th>
                    <th>{{tr('quantity')}}</th>
                    <th>{{tr('unit_cost')}}</th>
                    <th>{{tr('total')}}</th>
                </tr>
            </thead>
            <tbody>

            @foreach($invoice_items as $index => $items)

                <tr>
                    <td>{{ $index+1 }}</td>
                    <td>{{$items->name ?? '-'}}</td>
                    <td>{{$items->quantity ?? '0'}}</td>
                    <td>{{formatted_amount($items->sub_total ?? '0.00')}}</td>
                    <td>{{formatted_amount($items->total ?? '0.00')}}</td>
                </tr>
             @endforeach
            </tbody>
        </table>
    </div><br>
    <div class="row justify-content-end">

        <div class="col-xl-3 col-lg-4">
            <p class="text-dark mb-2">{{tr('terms')}}</p>
            <address>
               {{$invoice_details->terms_conditions ?? ''}}
            </address>

            <p class="text-dark mb-2">{{tr('notes')}}</p>
            <address>
               {{$invoice_details->notes ?? ''}}
            </address>
        </div>


        <div class="col-lg-5 col-xl-4 col-xl-3 ml-sm-auto">

            <ul class="list-unstyled mt-4">
                <li class="mid pb-3 text-dark"> {{ tr('sub_total')}}
                    <span class="d-inline-block float-right text-default">{{formatted_amount($invoice_details->sub_total??'0.00')}}</span>
                </li>
                <li class="mid pb-3 text-dark">{{tr('vat')}}
                    <span class="d-inline-block float-right text-default">{{formatted_amount($invoice_details->tax_total??'0.00')}}</span>
                </li>
                <li class="pb-3 text-dark">{{tr('total')}}
                    <span class="d-inline-block float-right">{{formatted_amount($invoice_details->total??'0.00')}}</span>
                </li>
            </ul>
            <!-- <a href="#" class="btn btn-block mt-2 btn-lg btn-primary btn-pill">{{tr('proceed_to_payment')}}</a> -->
        </div>
    </div>

    



</div>
    

@endsection