<div class="col-md-12 mb-2">

    <form class="col-sm-12 col-sm-offset-6 search_box_css" action="{{route('admin.user_disputes.index')}}" method="GET" role="search">

        <div class="row input-group">
            
            <div class="col-md-7"></div>

            <div class="col-md-2">
                <select class="form-control" name="status" >
                <option value="">{{tr('select_status')}}</option>
                <option value="{{DISPUTE_SENT}}" @if(Request::get('status') == DISPUTE_SENT) selected @endif>{{tr('DISPUTE_SENT')}}</option>   
                <option value="{{DISPUTE_APPROVED}}" @if(Request::get('status') == DISPUTE_APPROVED && Request::get('status')!='') selected @endif>{{tr('DISPUTE_APPROVED')}}</option>
                <option value="{{DISPUTE_DECLINED}}" @if(Request::get('status') == DISPUTE_DECLINED && Request::get('status')!='') selected @endif>{{tr('DISPUTE_DECLINED')}}</option>
                <option value="{{DISPUTE_CANCELLED}}" @if(Request::get('status') == DISPUTE_CANCELLED && Request::get('status')!='') selected @endif>{{tr('DISPUTE_CANCELLED')}}</option>
                </select>
            </div>

            <button type="submit" class="btn btn-info">
                <span class="glyphicon glyphicon-search"> {{tr('search')}}</span>
            </button> &nbsp;&nbsp;
            <a class="btn btn-danger" href="{{route('admin.user_disputes.index')}}">{{tr('clear')}}</a>
        </div>

    </form>
</div>