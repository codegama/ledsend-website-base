@extends('layouts.admin')

@section('page_header',tr('user_disputes'))

@section('breadcrumbs')

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('disputes')}}</li>
@endsection

@section('styles')

<style>
    .chat-img {
        width: 3rem;
        height: 3rem;
    }
</style>

@endsection

@section('content')

<div class="row no-gutters">
    <div class="col-lg-5 col-xxl-4">
        <div class="card card-default chat-left-sidebar">

            <div class="card-header">
                <h2>{{tr('dispute_details')}}</h2>
            </div>
            
            <div class="card-body" style="height: 645px;">
                <table class="table table-borderless table-thead-border">
                    <thead>
                        <tr>
                            <th>Label</th>
                            <th class="text-right">Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-dark font-weight-bold">{{tr('case_id')}}</td>
                            <td class="text-right">{{$user_dispute_details->user_dispute_unique_id}}</td>
                        </tr>

                        <tr>
                            <td class="text-dark font-weight-bold">{{tr('raised_by')}}</td>
                            <td class="text-right">
                                <a href="{{ route('admin.users.view', ['user_id' => $user_dispute_details->user_id]) }}">
                                    {{$user_dispute_details->DisputeSender->name ?? tr('not_available')}}
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-dark font-weight-bold">{{tr('receiver')}}</td>
                            <td class="text-right">
                                <a href="{{ route('admin.users.view', ['user_id' => $user_dispute_details->receiver_user_id]) }}">
                                    {{$user_dispute_details->DisputeReceiver->name ?? tr('not_available')}}
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-dark font-weight-bold">{{tr('amount')}}</td>
                            <td class="text-right">{{$user_dispute_details->amount_formatted}}</td>
                        </tr>

                        <tr>
                            <td class="text-dark font-weight-bold">{{tr('status')}}</td>
                            <td class="text-right">{{$user_dispute_details->status_formatted}}</td>
                        </tr>
                    </tbody>
                    <tfoot class="border-top">
                        
                    </tfoot>
                </table>

                @if(in_array($user_dispute_details->status, [DISPUTE_SENT]))

                <hr>

                <a href="{{route('admin.user_disputes.approve', ['user_dispute_id' => $user_dispute_details->user_dispute_id])}}" class="text-uppercase btn btn-success" onclick="return confirm('{{tr('are_you_sure')}}')">
                    {{tr('approve')}}
                </a>

                <a href="{{route('admin.user_disputes.reject', ['user_dispute_id' => $user_dispute_details->user_dispute_id])}}" class="text-uppercase btn btn-danger" onclick="return confirm('{{tr('are_you_sure')}}')">
                    {{tr('reject')}}
                </a>
            
                <hr>

                @endif

                <div>
                    <p class="text-dark font-weight-bold">{{tr('message')}}</p>
                    <p>{{$user_dispute_details->message ?? "-"}}</p>
                </div>

                <hr>

                <div>
                    <p class="text-dark font-weight-bold">{{tr('cancel_reason')}}</p>
                    <p>{{$user_dispute_details->cancel_reason ?? "-"}}</p>
                </div>

            </div>
        </div>
    </div>

    <div class="col-lg-7 col-xxl-8">
        <!-- Chat -->
        <div class="card card-default chat-right-sidebar">
            <div class="card-header">
                <h2>Conversations</h2>
            </div>

            <div class="card-body pb-0" data-simplebar style="height: 545px;">

                @include('admin.user-disputes._messages')

            </div>

            <div class="chat-footer">
                <form method="POST" action="{{route('admin.user_disputes.message_save')}}">
                    @csrf
                    <input type="hidden" name="user_dispute_id" value="{{$user_dispute_details->user_dispute_id}}">

                    <input type="hidden" name="user_wallet_payment_id" value="{{$user_dispute_details->user_wallet_payment_id}}">

                    <div class="input-group input-group-chat">
                        <div class="input-group-prepend">
                            <span class="emoticon-icon mdi mdi-email-outline"></span>
                        </div>
                        <input type="text" required name="message" class="form-control" aria-label="Enter message" />
                    </div>
                </form>
            </div>
        </div>
    
    </div>
</div>

@endsection